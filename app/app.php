<?php
use Silex\Application;
$app['debug'] = true;
$app = new Application();
$app['app.name'] = 'SilexApp';
$app['calc'] = function (){
    return new \SilexApp\Model\Services\Calculator();
};
$app['ddbb'] = function (){
    return new \SilexApp\Model\Services\DataBase();
};

$app['user'] = function (){
    return new \SilexApp\Model\Services\UserServiceProvider();
};

$app['img'] = function (){
    return new \SilexApp\Model\Services\ImgServiceProvider();
};

return $app;