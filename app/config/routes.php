<?php
//PROVES
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/*
 * Funcio que ens comprova que hi hagi
 * algun usuari iniciailitzat
 * Si no hi ha ningu inicialitzat, pagina ERROR
 */
$before = function (Request $request, Application $app){
    if (!$app['session']->has('id')){
        $response = new Response();
        $content = $app['twig']->render('error.twig', [
            'message' => 'Hauries d estar loggejat'
        ]);
        $response->setContent($content);
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        return $response;
    }
};

//PRACTICA
$app->get('/', 'SilexApp\\Controller\\HomeController::inici');
//PRACTICA - HOME
$app->match('/', 'SilexApp\\Controller\\HomeController::inici');
$app->match('/loadMoreImages', 'SilexApp\\Controller\\HomeController::moreImages');
$app->match('/loadMoreComments/{id}', 'SilexApp\\Controller\\ImageController::moreComments');


//IMATGE
$app->match('/newPic', 'SilexApp\\Controller\\ImageController::newPic')->before($before);
$app->match('/afegirPic', 'SilexApp\\Controller\\ImageController::afegirPic')->before($before);
$app->match('/editPic/{id}','SilexApp\\Controller\\ImageController::editPic')->before($before);
$app->match('/deletePic/{id}','SilexApp\\Controller\\ImageController::deletePic')->before($before);
$app->match('/actPic/{id}','SilexApp\\Controller\\ImageController::actualitzaImg');
$app->match('/viewImg/{id}', 'SilexApp\\Controller\\ImageController::viewPic');
$app->match('/comment/{id}', 'SilexApp\\Controller\\ImageController::addComment');
$app->match('/like/{id}', 'SilexApp\\Controller\\ImageController::likeImg');


//USUARI
$app->match('/log', 'SilexApp\\Controller\\UserController::login');
$app->match('/login', 'SilexApp\\Controller\\UserController::logUser');
$app->match('/reg', 'SilexApp\\Controller\\UserController::register');
$app->match('/register', 'SilexApp\\Controller\\UserController::regUser');
$app->match('/editProfile', 'SilexApp\\Controller\\UserController::edProfile');
$app->match('/activate', 'SilexApp\\Controller\\UserController::activaUser');
$app->match('/editP', 'SilexApp\\Controller\\UserController::edProfile')->before($before);
$app->match('/ed', 'SilexApp\\Controller\\UserController::compEdit');
$app->match('/Profile/{name}', 'SilexApp\\Controller\\UserController::Profile');
$app->match('/ordenaProfile/{name}', 'SilexApp\\Controller\\UserController::ordenaProfile');


$app->match('/myComments/{name}', 'SilexApp\\Controller\\UserController::Comments')->before($before);
$app->match('/deleteComment/{id}', 'SilexApp\\Controller\\UserController::deleteComment')->before($before);
$app->match('/editComment/{id}', 'SilexApp\\Controller\\UserController::editComment')->before($before);
$app->match('/Notifications/{name}', 'SilexApp\\Controller\\UserController::notifications')->before($before);
$app->match('/deleteNotification/{id}', 'SilexApp\\Controller\\UserController::deleteNotification')->before($before);
$app->match('/actCom/{id}', 'SilexApp\\Controller\\UserController::actComentari')->before($before);

//PROVES
$app->get('/hello/{name}', 'SilexApp\\Controller\\HelloController::indexAction');
$app->get('/add/{num1}/{num2}', 'SilexApp\\Controller\\HelloController::addAction');

//PROVES ENFOCADES A La PRACTICA
$app->get('/users/get/{id}/', 'SilexApp\\Controller\\UserController::getAction');
$app->match('/users/add','SilexApp\\Controller\\UserController::postAction');
//amb el match tan si ens entra un POST com un GET ens entrara


//exemple per sessions
//Aqui no fem per res amb la BBDD...


$app->get('/iniciSessio', 'SilexApp\\Controller\\BaseController::indexAction');
$app->get('/admin', 'SilexApp\\Controller\\BaseController::adminAction')->before($before);


