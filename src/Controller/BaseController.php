<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 24/04/2017
 * Time: 15:50
 */

namespace SilexApp\Controller;


use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController
{

    public function indexAction(Application $app){
    //metode has ens retorna un boolea ,el get si no hi es ens retorna un NULL
        if($app['session']->has('id')){
            $app['session']->remove('id');
            return new RedirectResponse("/");
        }

        $app['session']->set('name', 'Claudia');
        $app['session']->set('id', '1');

        $content = 'Sessio iniciada de: ' .$app['session']->get('name');
        return new Response($content);
    }

    public function adminAction(Application $app){
        $content = 'Benvingut un altre vegada ' .$app['session']->get('name');
        return new Response($content);
    }
}