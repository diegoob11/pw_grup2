<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 03/04/2017
 * Time: 16:18
 */

namespace SilexApp\Controller;

use Silex\Application;
//use Zend\Diactoros\Request;
use Symfony\Component\HttpFoundation\Request;
//use Zend\Diactoros\Response;
use Symfony\Component\HttpFoundation\Response;

class HelloController
{

    public function indexAction(Application $app, Request $request, $name){

        $content = $app['twig']->render('hello.twig', ['user' => $name]);
        $response = new Response();
        $response->setStatusCode($response::HTTP_OK);
        $response->headers->set('Content-type', 'text/html');
        $response->setContent($content);
        $response->setCache([
            's_maxage' => 3600
        ]);
        return $response;
    }

    public function addAction(Application $app, $num1, $num2){
        return "The result is " .$app['calc']->add($num1,$num2);
    }
}