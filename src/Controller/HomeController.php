<?php

namespace SilexApp\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class HomeController{

    public function inici(Application $app, Request $request){
        //Si la sessio esta iniciada...
        $logged = false;
        $nom = null;

        if($request->get('select') == null){
            $opcio = "value1";
        }else{
            $opcio = $request->get('select');
        }
        //Si la sessio esta iniciada...
        $logged = false;
        $nom = null;

        $img_list = $app['ddbb']->getPublicImg($app,$opcio );


        if ($app['session']->has('id')) {
            $logged = true;
            $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];

            $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
            $imgP = str_split($img_perfil,1);

            for($i = 11; $i < count($imgP); $i++){
                $nom[$i - 11] = $imgP[$i];
            }

            $nom = implode($nom);
            $content = $app['twig']->render('home.twig',[
                'self' => $self,
                'logged' => $logged,
                'items' => array_slice($img_list, 0, 5),
                'user_path' => "assets/img/3_$nom"
            ]);
        } else {
            $logged = false;
            $content = $app['twig']->render('home.twig',[
                'logged' => $logged,
                'items' => array_slice($img_list, 0, 5),
            ]);
        }

        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    public function moreImages(Application $app){

        $img = $app['ddbb']->getPublicImg($app, "value1");
        $j = 0;
        foreach($img as $i){
            $im_json = json_encode([
                'id' => $i->getId(),
                'nomUser' => $i->getNomUser(),
                'titol' => $i->getTitol(),
                'path' => $i->getPath(),
                'visites' => $i->getVisites(),
                'private' => $i->getPrivate(),
                'data' => $i->getData(),
                'coments' => $i->getComents(),
                'likes' => $i->getLikes(),
                'canLike' => $i->getCanLike(),
            ]);
            $images[$j] = $im_json;
            $j++;
        }
        return $json = json_encode($images);
    }
}