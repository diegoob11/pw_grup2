<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 24/04/2017
 * Time: 17:49
 */

namespace SilexApp\Controller;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use SilexApp\Model\Services\Img;
use SilexApp\Model\Services\Comentari;
use \Symfony\Component\HttpFoundation\RedirectResponse;

class ImageController
{

    /* Funcio que ens envia
     * a la part visual d'afegir img
     *
     */
    public function newPic (Application $app){
        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];

        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);

        $content = $app['twig']->render('newPic.twig', [
            'self' => $self,
            'error' => 0,
            'user_path' => "assets/img/3_$nom"
        ]);
        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    /*
     * Funcio que comprva
     * la info OK
     * i la guarda a la BBDD
     */
    public function afegirPic(Application $app, Request $request){
        $ok = $app['img']->creaImg($app, $request);
        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];

        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);

        if($ok == 0){
            return new RedirectResponse("/");

        }else{
            $content = $app['twig']->render('newPic.twig', [
                'self' => $self,
                'error' => $ok,
                'user_path' => "assets/img/3_$nom"
            ]);
        }

        $response = new Response();
        $response->setContent($content);
        return $response;
    }


    /*
     * Funcio que crida la VIEW
     * pèr a poder editar la imateg
     */
    public function editPic(Application $app, $id){
        $img = $app['ddbb']->getImgById($app, $id);

        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];

        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }
        $nom = implode($nom);

        $content = $app['twig']->render('editImg.twig', [
            'self' => $self,
            'user_path' => "../assets/img/3_$nom",
            'error' => 0,
            'img' => array(
                'titol' =>$img->getTitol(),
                'privat' => $img->getPrivate(),
                'id' => $img->getId(),
                'path' => $img->getPath()
            ),
        ]);
        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    public function actualitzaImg(Application $app, $id, Request $request){

        $ok = $app['img']->actualitzaImg($app, $request, $id);
        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];



        //TOK OK
        if($ok[0] == true){
            return new RedirectResponse("/Profile/$self");

        }else{
            if(strcmp($request->get('priv'), "on") == 0){
                $priv = 1;
            }else{
                $priv = 0;
            }
            $img = $app['ddbb']->getImgById($app, $id);
            $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
            $imgP = str_split($img_perfil,1);

            for($i = 11; $i < count($imgP); $i++){
                $nom[$i - 11] = $imgP[$i];
            }

            $nom = implode($nom);
            $content = $app['twig']->render('editImg.twig', [
                'error' => 1,
                'self' => $self,
                'user_path' => $nom,
                'img' => array(
                    'titol' =>$request->get('titol'),
                    'privat' => $priv,
                    'id' => $img->getId(),
                    'path' => $img->getPath()
                ),
            ]);
        }
        $response = new Response();
        $response->setContent($content);

        return $response;

    }

    /*
     * Funcio que elimina imatges
     * de la BBDD!
     */
    public function deletePic(Application $app, $id){

        $app['ddbb']->deleteComentsImg($app, $id);
        $app['ddbb']->deleteLikesImg($app, $id);
        $app['ddbb']->deleteNotisFromImage($app, $id);
        $app['ddbb']->deleteImageRe($app,$id);
        $app['ddbb']->deleteImg($app, $id);



        return new RedirectResponse("/Profile/".$app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari']);

    }

    public function viewPic(Application $app, $id){
        $logged = false;

        if ($app['session']->has('id')) {
            $logged = true;
            $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id']);

            $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
            $imgP = str_split($img_perfil,1);

            for($i = 11; $i < count($imgP); $i++){
                $nom[$i - 11] = $imgP[$i];
            }

            $nom = implode($nom);
        } else {
            $logged = false;
        }

        $app['ddbb']->imageSeen($app, $id);
        $img = $app['ddbb']->getImgbyId($app, $id);
        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id']);
        if(!empty($img->getComents())) {
            $comments = count($img->getComents());
        }else{
            $comments = 0;
        }
        $today = time();
        $date = strtotime($img->getData());
        $diff = floor(($today - $date)/(60 * 60 * 24));
        if(!empty($img->getComents())){
            $comm = array_slice($img->getComents(), 0, 3);
        }else{
            $comm = $img->getComents();
        }
        $content = $app['twig']->render('viewImg.twig',[
            'self' => $self['usuari'],
            'user_path' => "../assets/img/3_$nom",
            'self-id' => $app['session']->get('id')['id'],
            'logged' => $logged,
            'item' => $img,
            'comment_num' => $comments,
            'comments' => $comm,
            'days' => $diff,
        ]);
        $response = new Response();
        if($img->getPrivate() == 1){
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            $content = $app['twig']->render('error.twig', [
                'message' => 'Hauries d estar loggejat'
            ]);
        }
        $response->setContent($content);
        return $response;
    }

    /*
     * Funcio on si el usr encara
     * no ha comentat una foto, la pugui comentar
     */
    public function addComment(Application $app, Request $request, $id){
       $app['img']->comentari($app, $id, $request);
        return new RedirectResponse("/");
    }

    /*
     * Funcio que posa like a les fotos
     */
    public function likeImg(Application $app, $id){
        $app['img']-> like($app, $id);
        return new RedirectResponse("/");
    }

    public function moreComments(Application $app, $id){
        $img = $app['ddbb']->getImgbyId($app, $id );
        $j = 0;
        $comments = $img->getComents();
        foreach($comments as  $i){
            $com_json = json_encode([
                'id' => $i->getId(),
                'nomUser' => $i->getNomUser()['usuari'],
                'id_img' => $i->getIdImg(),
                'comentari' => $i->getComentari(),
                'data' => $i->getData(),
            ]);
            $com[$j] = $com_json;
            $j++;
        }
        return $json = json_encode($com);
    }
}