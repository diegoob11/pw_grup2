<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 19/04/2017
 * Time: 18:58
 */

namespace SilexApp\Controller;
use Doctrine\DBAL\DriverManager;
use Silex\Application;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SilexApp\Model\Services\DataBase;
use SilexApp\Model\Services\User;
use SilexApp\Model\Services\UserServiceProvider;

class UserController
{
    /*
     * Funció que ens crida a la VIEW per a
     * poder loggejar-nos
     */
    public function login(Application $app){
        $content = $app['twig']->render('login.twig', [
            'logged' => 2,
        ]);
        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    /*
     * Funció que ens comprova que les dades del LOGGIN
     * siguin correctes i ens redirigeix on faci falta
     */
    public function logUser(Application $app, Request $request){
        $error = $app['user']->comprovaUser($app, $request);

        //INICIAR SESSIO
        if ($error == 2){

            return new RedirectResponse("/");
        }else{
            $content = $app['twig']->render('login.twig', [
                'logged' => $error,
            ]);
        }
        $response = new Response();
        $response->setContent($content);
        return $response;

    }

    /*
     * Funcio que ens crida a la VIEW per a
     * poder fer el registre del nou user
     */
    public function register(Application $app){
        //per saber si hi ha algun error + si sha de mostrar el link
        $vectorErrors[0] = false;
        $vectorErrors[1] = false;
        $vectorErrors[2] = false;
        $vectorErrors[3] = false;
        $vectorErrors[4] = false;


        $content = $app['twig']->render('register.twig', [
            'errors' =>  $vectorErrors,
            'valors' => array(
                'nom' =>"",
                'email' => "",
                'pass' => "",
                'pass2' =>"",
                'date' =>"",
                'img' => "",
            ),
            'activar' => false,
        ]);


        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    /*
     * Funcio que ens comprova totes les dades introduides
     * i ens fa el check in o no
     */
    public function regUser(Application $app, Request $request){

       $vectorErrors = $app['user']->registrar($request, $app);

        if($vectorErrors == null) {
            //OK
            $content = $app['twig']->render('register.twig', [
                'errors' =>  $vectorErrors,
                'valors' => array(
                    'nom' =>"",
                    'email' => "",
                    'pass' => "",
                    'pass2' =>"",
                    'date' =>"",
                    'img' => "",
                ),
                'activar' => true,
                ]);
        }else{
            $content = $app['twig']->render('register.twig', [
                'errors' => $vectorErrors,
                'valors' => array(
                    'nom' => $request->get('username'),
                    'email' => $request->get('email'),
                    'pass' => $request->get('password'),
                    'pass2' => $request->get('repeat_pwd'),
                    'date' => $request->get('dataN'),
                    'img' => $request->get('img'),
                ),
                'activar' => false,
            ]);
        }

        $response = new Response();
        $response->setContent($content);
        return $response;

    }

    /*
     * Funcio que posa a 1 l'atribut "actiu" de l'usuari
     * i guarda el id del usr en la sessio
     */
    public function activaUser(Application $app){
       $ok =  $app['user']->activaUser($app);


        return new RedirectResponse("/");


}

    /*
     * Crida a la VIEW de Editor de perfil
     */
    public function edProfile(Application $app, Request $request){

        //per saber si hi ha algun error + si sha de mostrar el link
        $vectorErrors[0] = false;
        $vectorErrors[1] = false;
        $vectorErrors[2] = false;
        $vectorErrors[3] = true;
        $vectorErrors[4] = false;

        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];

        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);


        $content = $app['twig']->render('editProfile.twig', [
            'errors' =>  $vectorErrors,
            'self' => $self,
            'user_path' => "../assets/img/3_$nom",
            'valors' => array(
                'nom' =>"",
                'pass' => "",
                'date' =>"",
                'img' => "",
            ),
            'missatge' => 2
        ]);


        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    /*
     * Compriva que la info
     * per a modificar la info del perfil
     * sigui OK
     */
    public function compEdit(Application $app, Request $request){

        $miss = 0;
        $vectorErrors = $app['user']->comprovaNouPerfil($app, $request);
        //NO error

        if(count($vectorErrors) == 1 && $vectorErrors == 1 ) {
           //PENJAT OK --> mostrar info
               $content = $app['twig']->render('editProfile.twig', [
                   'errors' => $vectorErrors,
                   'valors' => array(
                       'nom' =>$request->get('username'),
                       'pass' => $request->get('password'),
                       'date' =>$request->get('DataN'),
                       'img' => "",
                   ),
                   'missatge' => 1
               ]);
       }


        if(count($vectorErrors) > 1 || $vectorErrors == 0){
            //ERROR --> mostrar error
            $content = $app['twig']->render('editProfile.twig', [
                'errors' => $vectorErrors,
                'valors' => array(
                    'nom' =>"",
                    'pass' => "",
                    'date' =>"",
                    'img' => "",
                ),
                'missatge' => 0

            ]);
        }

        $response = new Response();
        $response->setContent($content);

        return $response;
    }

    /**
     *Crida la VIEW del Perfil
     */
    public function Profile (Application $app, $name){
        //Hem de fer un get de la BBDD per poder carregar la info del usuari
        $id = $app['ddbb']->getUserId($app, $name);
        $numComent = 0;
        $numImg = 0;
        $select = false;

        $img_list = $app['ddbb']->getUserImgOrdenat($app, $id, "value1");

        $idSessio = implode($app['session']->get('id'));

        if(strcmp($id['id'],$idSessio)== 0){
            $select = false;
        }else{
            $select = true;
            $numImg = count($img_list);
            $numComent = $app['ddbb']->comptaTotalComents($app, $img_list);
        }

        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];


        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);

        $content = $app['twig']->render('Profile.twig', [
            'self' => $self,
            'user_path' => "../assets/img/3_$nom",
            'item' => $img_list,
            'select' => $select,
            'numImg' => $numImg,
            'numCom' => $numComent,
            'nom' => $name
        ]);

        $response = new Response();
        $response->setContent($content);

        return $response;
    }

    /*
     * CRida la VIEW de perfil
     * amb les imatges reordenades
     */
    public function ordenaProfile (Application $app, $name, Request $request){
        //Hem de fer un get de la BBDD per poder carregar la info del usuari
        $id = $app['ddbb']->getUserId($app, $name);
        $numComent = 0;
        $numImg = 0;

        $img_list = $app['ddbb']->getUserImgOrdenat($app, $id, $request->get('select'));

        //TODO ORDENAR IMATGES PER NUM COMENTARIS i LIKES

        $select = false;
        $numImg = count($img_list);
        $numComent = $app['ddbb']->comptaTotalComents($app, $img_list);

        $self = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];


        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);


        $content = $app['twig']->render('Profile.twig', [
            'self' => $self,
            'user_path' => "../assets/img/3_$nom",
            'item' => $img_list,
            'select' => true,
            'numImg' => $numImg,
            'numCom' => $numComent,
            'nom' => $name
        ]);

        $response = new Response();
        $response->setContent($content);

        return $response;
    }

    public function Comments(Application $app, $name){
        $id = $app['ddbb']->getUserId($app, $name);
        $comments = $app['ddbb']->getCommentsFromUser($app, $id['id']);

        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);

        $content = $app['twig']->render('Comments.twig', [
            'user_path' => "../assets/img/3_$nom",
            'comments' => $comments,
            'user' => $name,
        ]);

        $response = new Response();
        $response->setContent($content);

        return $response;
    }

    public function deleteComment(Application $app, $id){
        $comments = $app['ddbb']->deleteComment($app, $id);

        return new RedirectResponse("/");
    }

    public function editComment (Application $app, $id){

        $com = $app['ddbb']->getComentById($app,$id);
        $img = $app['ddbb']->getImgById($app, $com['id_img']);
        $titol = $img->getTitol();
        $usr = $img->getNomUser()['usuari'];
        $text = "Comment on image $titol , from $usr";
        $content = $app['twig']->render('editComent.twig', [
            'text' => $text,
            'id' => $id,
            'comment' => $com['comentari'],
        ]);

        $response = new Response();
        $response->setContent($content);

        return $response;
    }

    public function actComentari (Application $app, $id, Request $request){
        $app['ddbb']->actualitzaComentari($app, $id,$request->get('com'));

        $usr = $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'];

        return new RedirectResponse("/myComments/$usr");

    }

    public function notifications(Application $app, $name){
        $id = $app['ddbb']->getUserId($app, $name);
        $notifications = $app['ddbb']->getUserNotifications($app, $id['id']);
        $noti = $app['user']->organitzaNotis($app, $notifications);

        $img_perfil = $app['ddbb']->getImgPerfil($app,  $app['session']->get('id')['id'] );
        $imgP = str_split($img_perfil,1);

        for($i = 11; $i < count($imgP); $i++){
            $nom[$i - 11] = $imgP[$i];
        }

        $nom = implode($nom);

        $content = $app['twig']->render('Notifications.twig', [
                'user_path' => "../assets/img/3_$nom",
                'self' => $name,
                'notifications' => $noti,
            ]
        );
        $response = new Response();
        $response->setContent($content);
        return $response;
    }

    public function deleteNotification(Application $app, $id){
        $ok = $app['ddbb']->deleteNoti($app, $id);

        return new RedirectResponse("/");
    }
//---------------------PROVES DE CLASSE--------------------------

    public function getAction(Application $app, $id){
        $sql = "SELECT * FROM user WHERE id = ?";
        $user = $app['db']->fetchAssoc($sql, array((int)$id));
        $response = new Response();
        if (!$user) {
            $response->setStatusCode(Response::HTTP_NOT_FOUND);
            $content = $app['twig']->render('error.twig', [
                    'message' => 'User not found'
                ]
            );

        } else {
            $response->setStatusCode(Response::HTTP_OK);
            $content = $app['twig']->render('user.twig', [
                    'user' => $user
                ]
            );
        }
        $response->setContent($content);
        return $response;
    }

    public function postAction(Application $app, Request $request){

        $response = new Response();

        if($request->isMethod('POST')){
            //VALIDEM
            $name = $request->get('name');
            $email = $request->get('email');
            try{
                $app['db']->insert('user', [
                    'name' => $name,
                    'email' => $email
                    ]
                );

                $lastInsertedId = $app['db']->fetchAssoc('SELECT id FROM user ORDER BY id DESC LIMIT 1');
                $id = $lastInsertedId['id'];
                $url = '/users/get/' . $id;
                return new RedirectResponse($url);
            }catch (Exception $e){
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                $content = $app['twig']->render('user.twig', [
                    'errors' => [
                        'unexpected' => 'hi ha hagut un error'
                    ]
                ]);

                $response->setContent($content);
                return $response;
            }

        }
        $content = $app['twig']->render('user.twig',array(
            'app' => [
                'name' => $app['app.name']
            ]
        ));
        $response->setContent($content);
        return $response;
    }
}