<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 03/05/2017
 * Time: 20:39
 */

namespace SilexApp\Model\Services;


class Comentari
{
    private $id;
    private $nomUser;
    private $id_img;
    private $comentari;
    private $data;

    /**
     * Img constructor.
     * @param $id
     * @param $nomUser
     * @param $id_img
     * @param $comentari
     * @param $coments
     */
    public function __construct($id, $nomUser, $id_img, $comentari, $data)
    {
        $this->id = $id;
        $this->nomUser = $nomUser;
        $this->id_img = $id_img;
        $this->comentari = $comentari;
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomUser()
    {
        return $this->nomUser;
    }

    /**
     * @param mixed $nomUser
     */
    public function setNomUser($nomUser)
    {
        $this->nomUser = $nomUser;
    }

    /**
     * @return mixed
     */
    public function getIdImg()
    {
        return $this->id_img;
    }

    /**
     * @param mixed $id_img
     */
    public function setIdImg($id_img)
    {
        $this->id_img = $id_img;
    }

    /**
     * @return mixed
     */
    public function getComentari()
    {
        return $this->comentari;
    }

    /**
     * @param mixed $comentari
     */
    public function setComentari($comentari)
    {
        $this->comentari = $comentari;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }






}