<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 27/04/2017
 * Time: 18:56
 */

namespace SilexApp\Model\Services;

use Silex\Application;
use SilexApp\Model\Services\Img;
use SilexApp\Model\Services\User;
use SilexApp\Model\Services\Comentari;
use Symfony\Component\HttpFoundation\Request;

class DataBase
{
//-------------------USER------------------------------------------
    /*
     * agafa la info de l'usuari des del seu $id
     */
    public function afegirUser(Application $app, User $user){

        $app['db']->insert('usuari', [
                'usuari' => $user->getUsername(),
                'email' => $user->getEmail(),
                'data_neix' => $user->getDataNeix(),
                'contrasenya' => $user->getContra(),
                'img_perfil' => $user->getImgPerfil(),
                'actiu' => 0
            ]
        );

    }

    /*
     * Funcio que activa l'usuari
     * REtorna 1 si ho ha fet BE
     */
    public function activaUser(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();

        $qb->update('usuari')
            ->set('usuari.actiu', 1)
            ->where('usuari.id = :id')
            ->setParameter('id', (int)$id);

        $ok = $qb->execute();

        return $ok;
    }

    /*
     * Funcio que acutalitza el nom del user
     */
    public function actualitzaUsername(Application $app, $usu){
        $id = $app['session']->get('id');

        $qb = $app['db']->createQueryBuilder();

        $qb->update('usuari')
            ->set('usuari.usuari', ':usu')
            ->where('usuari.id = :id')
            ->setParameter('id', (int)$id['id'])
            ->setParameter('usu', $usu);

        $ok = $qb->execute();

        return $ok;
    }
    /*
     * Funcio que acutalitza la pass del user
     */
    public function actualitzaUserPass(Application $app, $pass){
        $id = $app['session']->get('id');

        $qb = $app['db']->createQueryBuilder();

        $qb->update('usuari')
            ->set('usuari.contrasenya', ':pass')
            ->where('usuari.id = :id')
            ->setParameter('id', (int)$id['id'])
            ->setParameter('pass', $pass);

        $ok = $qb->execute();

        return $ok;
    }
    /*
     * Funcio que acutalitza la data del user
     */
    public function actualitzaUserDate(Application $app, $date){
        $id = $app['session']->get('id');

        $qb = $app['db']->createQueryBuilder();

        $qb->update('usuari')
            ->set('usuari.data_neix', ':date')
            ->where('usuari.id = :id')
            ->setParameter('id', (int)$id['id'])
            ->setParameter('date', $date);

        $ok = $qb->execute();

        return $ok;
    }

    /*
     * Funcio que actualitza la img del user
     */
    public function actualitzaUserImg(Application $app, $img){
        $id = $app['session']->get('id');

        $qb = $app['db']->createQueryBuilder();

        $qb->update('usuari')
            ->set('usuari.img_perfil', ':img')
            ->where('usuari.id = :id')
            ->setParameter('id', (int)$id['id'])
            ->setParameter('img', $img);

        $ok = $qb->execute();

        return $ok;
    }
    /*
     * Funcio que ens indica si les dades son correctes
     * Retorna 0 si no existeix aquell usuari
     *         1 si la contrasenya es incorrecte
     *         2 si login es OK
     */
    public function loginUser(Application $app, $user, $pass){

        $sql = "SELECT contrasenya , actiu FROM usuari WHERE usuari = ? OR email = ?";

        $usuari = $app['db']->fetchAssoc($sql, array($user, $user));

        if(!$usuari){
            return 0;
        }else{
            if(strcmp($usuari['contrasenya'], $pass) != 0) {
                return 1;
            }else{
                return 2;
            }
        }

    }

    public function getLastId(Application $app){
        $lastInsertedId = $app['db']->fetchAssoc('SELECT id FROM usuari ORDER BY id DESC LIMIT 1');
        $id = $lastInsertedId['id'];

        return $id;
    }

    public function getUserId(Application $app, $user){
        $sql = "SELECT id FROM usuari WHERE usuari = ? OR email = ?";
        $id = $app['db']->fetchAssoc($sql, array($user, $user));
        return $id;
    }
    /*
    * Funcio que ens retorna el nom d'un
    * usuari en especifi
    */
    public function getUserName(Application $app, $id){

        $sql = "SELECT usuari FROM usuari WHERE id = ?";
        $user = $app['db']->fetchAssoc($sql, array((int)$id));

        return $user;
    }

    //--------------------IMG---------------------------------
    /*
     * Funcio que ens retorna totes les img
     * publiques guardades a la bbdd
     * SINO hi ha img retornem NULL
     */
    public function getPublicImg(Application $app, $opcio){
        $imgVector = NULL;
        switch ($opcio){
            case "value1":
                $sql = "SELECT * FROM imatges WHERE privat = 0 ORDER BY data_creacio DESC ";
                break;
            case "value2":
                $sql = "SELECT * FROM imatges WHERE privat = 0 ORDER BY visites DESC ";
        }
        $img = $app['db']->fetchAll($sql);

        for($i = 0; $i < count($img); $i++){
            $user = $this->getUserName($app, $img[$i]['id_usu']);
            $likes = $this->getLikesImg($app, $img[$i]['id']);
            $canLike = $this->likeDone($app, $img[$i]['id']);
            $path = str_split($img[$i]['img_path'],1);

            for($j = 0; $j < 11; $j++){
                $newPath[$j] = $path[$j];
            }
            $newPath[11] = "1";
            $newPath[12] = ".";
            for($j = 11; $j < count($path); $j++){
                $newPath[$j+2] = $path[$j];
            }
            $comentaris = $this->getCommentsImg($app, $img[$i]['id']);
            if(!$comentaris){
                $comentaris = null;
            }
            $novaImatge = new Img($img[$i]['id'],$user['usuari'],$img[$i]['titol'],$img[$i]['img_path'],$img[$i]['visites'],
                        $img[$i]['privat'],$img[$i]['data_creacio'],$comentaris,$likes, $canLike);
            $imgVector[$i] = $novaImatge;
        }

        return $imgVector;
    }

    public function getLastIdImg(Application $app){
        $lastInsertedId = $app['db']->fetchAssoc('SELECT id FROM imatges ORDER BY id DESC LIMIT 1');
        $id = $lastInsertedId['id'];

        if($id == null){
            $id = 0;
        }
        return $id;
    }

    public function deleteImageRe(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();
        $qb->delete('imatgesre')
            ->where('imatgesre.id_img = :id')
            ->setParameter('id', $id);
        $ok = $qb->execute();

        return $ok;
    }
    /*
     * Funcio que ens retorna un array amb totes
     * les imatges d'un usuari en especif
     * SINO hi ha img retornem NULL
     */
    public function getUserImg(Application $app, $id){
        $sql = "SELECT * FROM imatges WHERE id_usu = ?";
        $img = $app['db']->fetchAll($sql, array($id['id']));
        $imgVector = NULL;

        if(!$img){
            return $imgVector;
        }else{
            $user = $this->getUserName($app, $app['session']->get('id'));

            for($i = 0; $i < count($img); $i++){
                $comentaris = $this->getCommentsImg($app, $img[$i]['id']);
                $likes = $this->getLikesImg($app, $img[$i]['id']);
                $canLike = $this->likeDone($app, $img[0]['id']);
                if(!$comentaris){
                    $comentaris = null;
                }
                $novaImatge = new Img($img[$i]['id'],$user['usuari'],$img[$i]['titol'],$img[$i]['img_path'],$img[$i]['visites'],
                    $img[$i]['privat'],$img[$i]['data_creacio'],$comentaris,$likes, $canLike);
                $imgVector[$i] = $novaImatge;
            }
        }


        return $imgVector;
    }

    /*
     * Funcio que guarda les imatges
     * amb mida 400x300 i 100x100
     * opcio = 0-> perfil 1->pic
     */
    public function addImgRe(Application $app, $id, $img, $opcio){
        switch ($opcio){
            case 0 :
                $ok = $app['db']->insert('imatgesre', [
                        'id_usu' => $id,
                        'id_img' => 0,
                        'path' => $img
                    ]
                );
                break;
            case 1:
                $ok = $app['db']->insert('imatgesre', [
                        'id_img' => $id,
                        'id_usu' => 0,
                        'path' => $img
                    ]
                );
                break;
        }

        return $ok;


    }

    /*
     * Funcio que ens retornar la imatge
     * que pertany al id que hem passat
     */
    public function getImgById(Application $app, $id){
        $sql = "SELECT * FROM imatges WHERE id = ?";
        $img = $app['db']->fetchAll($sql, array((int)$id));
        $novaImg = null;
        if($img){
            $usu = $this->getUserName($app, $img[0]['id_usu']);
            $likes = $this->getLikesImg($app, $img[0]['id']);
            $canLike = $this->likeDone($app, $img[0]['id']);
            $comentaris = $this->getCommentsImg($app, $img[0]['id']);

            $novaImg = new Img($img[0]['id'], $usu,$img[0]['titol'], $img[0]['img_path'], $img[0]['visites'], $img[0]['privat'],$img[0]['data_creacio'], $comentaris,$likes, $canLike);
        }

        return $novaImg;
    }

    public function afegirImg(Application $app, Img $img){

        $ok = $app['db']->insert('imatges', [
                'id_usu' => (int)$app['session']->get('id')['id'],
                'titol' => $img->getTitol(),
                'img_path' => $img->getPath(),
                'visites' => 0,
                'privat' => $img->getPrivate(),
                'data_creacio' => $img->getData(),
            ]
        );

        return $ok;
    }

    public function deleteImg(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();
        $qb->delete('imatges')
            ->where('imatges.id = :id')
            ->setParameter('id', $id);
        $ok = $qb->execute();

        return $ok;

    }

    public function actualitzaImg(Application $app, Request $request,  $id){

        $qb = $app['db']->createQueryBuilder();

        if(strcmp($request->get('priv'), "on") == 0){
            $priv = 1;
        }else{
            $priv = 0;
        }
        $qb->update('imatges')
            ->set('imatges.titol', ':titol')
            ->set('imatges.privat', ':priv')
            ->where('imatges.id = :id')
            ->setParameter('titol', $request->get('titol'))
            ->setParameter('priv', $priv)
            ->setParameter('id', (int)$id);


        $ok = $qb->execute();

        return $ok;
    }

    public function imageSeen(Application $app, $id){
        $sql = "SELECT visites FROM imatges WHERE id = ? LIMIT 1";
        $vis = $app['db']->fetchAll($sql, array((int)$id));
        $vis[0]['visites'] += 1;
        $val = $vis[0]['visites'];
        $qb = $app['db']->createQueryBuilder();
        $qb->update('imatges')
            ->set('imatges.visites', $val)
            ->where('imatges.id = :id')
            ->setParameter('id', $id);
        $qb->execute();
    }

    public function getUserImgOrdenat(Application $app, $id, $opcio){
        switch ($opcio){
            case "value1":
                $sql = "SELECT * FROM imatges WHERE id_usu = ? ORDER BY data_creacio DESC ";
                break;
            case "value3":
                $sql = "SELECT imatges.*, COUNT(comentaris.id) AS views
                        FROM imatges LEFT JOIN comentaris ON imatges.id = comentaris.id_img
                        GROUP BY imatges.id
                        ORDER BY views DESC";
                break;
            case "value2":
                $sql = "SELECT imatges.*, COUNT(likes.id) AS views
                        FROM imatges LEFT JOIN likes ON imatges.id = likes.id_img
                        GROUP BY imatges.id
                        ORDER BY views DESC";
                break;
            case "value4":
                $sql = "SELECT * FROM imatges WHERE id_usu = ? ORDER BY visites DESC ";
        }

        $img = $app['db']->fetchAll($sql, array($id['id']));
        $imgVector = NULL;

        if(!$img){
            return $imgVector;
        }else{
            $user = $this->getUserName($app, $app['session']->get('id'));

            for($i = 0; $i < count($img); $i++){
                $likes = $this->getLikesImg($app, $img[0]['id']);
                $canLike = $this->likeDone($app, $img[0]['id']);
                $comentaris = $this->getCommentsImg($app, $img[$i]['id']);
                if(!$comentaris){
                    $comentaris = null;
                }
                $novaImatge = new Img($img[$i]['id'],$user['usuari'],$img[$i]['titol'],$img[$i]['img_path'],$img[$i]['visites'],
                    $img[$i]['privat'],$img[$i]['data_creacio'],$comentaris,$likes, $canLike);
                $imgVector[$i] = $novaImatge;
            }
        }

        return $imgVector;

    }

    /*
    * Funcio que ens retorna la path de la imatge
    */
    public function getPathImg(Application $app, $id_usu){
        $sql = "SELECT * FROM usuari WHERE id = ?";
        $img = $app['db']->fetchAll($sql, array((int)$id_usu));
        return $img[0]['img_perfil'];
    }

    public function getMyTitleImg(Application $app, $id){
        $sql = "SELECT titol FROM imatges WHERE id = ?";
        $titol = $app['db']->fetchAll($sql, array($id));
        return $titol;
    }
    //------------------COMENTARIS------------------------

    /*
     * Funcio que ens retorna un array amb tots els comentaris
     * que s'han fet a una imatge en especific
     * SINO hi ha coments retornem NULL
     */
    public function getCommentsImg(Application $app, $id_img){
        $sql = "SELECT * FROM comentaris WHERE id_img = ?";
        $coment = $app['db']->fetchAll($sql, array((int)$id_img));

        $ComenVector = NULL;

        if(!$coment){
            return $ComenVector;
        }else{
            for($i = 0; $i < count($coment); $i++){
                $user = $this->getUserName($app, $coment[$i]['id_usu']);
                $nouComent = new Comentari($coment[$i]['id'],$user,$coment[$i]['id_img'],$coment[$i]['comentari'],$coment[$i]['data_creacio']);
                $ComenVector[$i] = $nouComent;
            }
        }

        return $ComenVector;

    }

    public function actualitzaComentari(Application $app, $id, $com){

        $qb = $app['db']->createQueryBuilder();

        $qb->update('comentaris')
            ->set('comentaris.comentari', ':com')
            ->where('comentaris.id = :id')
            ->setParameter('id', (int)$id)
            ->setParameter('com', $com);

        $ok = $qb->execute();
        return $ok;
    }

    /*
     * Fuincio que afegeix un comentari a una imatge
     */
    public function afegirCommentsImg(Application $app, Comentari $com){

        $nom = $com->getNomUser();

        $app['db']->insert('comentaris', [
                'id_usu' => $nom,
                'id_img' => $com->getIdImg(),
                'comentari' => $com->getComentari(),
                'data_creacio' => $com->getData(),
            ]
        );
    }

    public function getCommentsFromUser(Application $app, $id){
        $sql = "SELECT * FROM comentaris WHERE id_usu = ?";
        $comment = $app['db']->fetchAll($sql, array((int)$id));
        $ComenVector = NULL;

        if(!$comment){
            return $ComenVector;
        }else{
            for($i = 0; $i < count($comment); $i++){
                $user = $this->getUserName($app, $comment[$i]['id_usu']);
                $nouComent = new Comentari($comment[$i]['id'],$user,$comment[$i]['id_img'],
                    $comment[$i]['comentari'],$comment[$i]['data_creacio']);
                $ComenVector[$i] = $nouComent;
            }
        }
        return $ComenVector;
    }

    public function deleteComment(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();
        $qb->delete('comentaris')
            ->where('comentaris.id = :id')
            ->setParameter('id', $id);
        $ok = $qb->execute();

        return $ok;

    }

    public function comptaTotalComents(Application $app,  $imgVect){
        $num = 0;

        for ($i = 0; $i < count($imgVect); $i++){
            $com = count($imgVect[$i]->getComents());
            if($com == NULL){
                $com = 0;
            }
            $num = $num + $com;
        }
        return $num;
    }

    public function getComentById(Application $app, $id){
        $sql = "SELECT * FROM comentaris WHERE id = ? ";
        $com = $app['db']->fetchAssoc($sql, array($id));
        return $com;
    }
    /*
     * Funcio que elimina tots els comentaris
     * d'una imatge
     */
    public function  deleteComentsImg(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();
        $qb->delete('comentaris')
            ->where('comentaris.id_img = :id')
            ->setParameter('id', $id);
        $ok = $qb->execute();

        return $ok;

    }

    /*
    * Funcio que ens mira
    * si JO com a usr ja he
    * fet comentat a una img
    * RETORNA false si ja esta fet el comentari
    */
    public function commentDone(Application $app, $id){
        $id_usu = $app['session']->get('id')['id'];

        $sql = "SELECT * FROM comentaris WHERE id_usu = ? AND id_img = ?";
        $com = $app['db']->fetchAll($sql, array($id_usu, (int)$id));

        if(empty($com)){
            return true;
        }else{
            return false;
        }
    }

    //--------------------LIKES---------------------------

    /*
     * Funcio que elimina
     * tots els likes d'una imatge
     */
    public function deleteLikesImg(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();
        $qb->delete('likes')
            ->where('likes.id_img = :id')
            ->setParameter('id', $id);
        $ok = $qb->execute();

        return $ok;
    }

    /*
     * Funcio que afegeix 1 like
     */
    public  function afegirLike(Application $app, $id){
        $id_usu = $app['session']->get('id')['id'];

        $app['db']->insert('likes', [
                'id_usu' => $id_usu,
                'id_img' => $id,
            ]
        );
    }

    /*
     * Funcio que elimina 1 like
     * d'una foto especifica
     */
    public function delete1Like(Application $app, $id){
        $id_usu = $app['session']->get('id')['id'];

        $qb = $app['db']->createQueryBuilder();

        $qb->delete('likes')
            ->where('likes.id_img = :id AND likes.id_usu =:id_usu')
            ->setParameter('id', $id)
            ->setParameter('id_usu', $id_usu);
        $ok = $qb->execute();

        return $ok;
    }

    /*
     * Funcio que ens mira
     * si JO com a usr ja he
     * fet like a una img
     * RETORNA false si ja esta fet el comentari
     */
    public function likeDone(Application $app, $id){
        $id_usu = $app['session']->get('id')['id'];

        $sql = "SELECT * FROM likes WHERE id_usu = ? AND id_img = ?";
        $img = $app['db']->fetchAll($sql, array($id_usu, (int)$id));

        if(empty($img)){
            return true;
        }else{
            return false;
        }
    }

    public function getLikesImg(Application $app, $id_img){
        $sql = "SELECT * FROM likes WHERE id_img = ?";
        $img = $app['db']->fetchAll($sql, array((int)$id_img));

        return count($img);

    }

    //---------------NOTIFICATIONS----------------------------

    //-->CL -> 0 Add coment 1-> deleteComent 2->addLike 3->delteLike
    public function getUserNotifications(Application $app, $id){
        $sql = "SELECT * FROM notificacions WHERE id_usu = ?";
        $noti = $app['db']->fetchAll($sql, array($id));

        return $noti;

    }

    public function insertNotification(Application $app,$CL, $id_img, $id_usu, $id_com){

        $app['db']->insert('notificacions', [
                'id_usu' => $id_usu['id'],
                'id_img' =>$id_img,
                'id_commented' => $id_com,
                'CL' => $CL,
            ]
        );
    }

    public function deleteNoti(Application $app, $id){

        $qb = $app['db']->createQueryBuilder();

        $qb->delete('notificacions')
            ->where('notificacions.id = :id ')
            ->setParameter('id', $id);
        $ok = $qb->execute();
        return $ok;
    }

    public function deleteNotisFromImage(Application $app, $id){
        $qb = $app['db']->createQueryBuilder();

        $qb->delete('notificacions')
            ->where('notificacions.id_img = :id_img ')
            ->setParameter('id_img', $id);
        $ok = $qb->execute();
        return $ok;
    }

    public function getImgPerfil(Application $app, $id){
        $sql = "SELECT img_perfil FROM usuari WHERE id = ?";
        $img = $app['db']->fetchAssoc($sql, array((int)$id))['img_perfil'];
        return $img;
    }
}