<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 30/04/2017
 * Time: 13:22
 */

namespace SilexApp\Model\Services;


class Img
{
    private $id;
    private $nomUser;
    private $titol;
    private $path;
    private $visites;
    private $private;
    private $data;
    private $coments;
    private $likes;
    private $canLike;

    /**
     * Img constructor.
     * @param $id
     * @param $nomUser
     * @param $titol
     * @param $path
     * @param $visites
     * @param $private
     * @param $data
     */
    public function __construct($id, $nomUser, $titol, $path, $visites, $private, $data, $coments, $likes, $canLike)
    {
        $this->id = $id;
        $this->nomUser = $nomUser;
        $this->titol = $titol;
        $this->path = $path;
        $this->visites = $visites;
        $this->private = $private;
        $this->data = $data;
        $this->coments = $coments; //SINO hi ha comentaris, sera NULL
        $this->likes = $likes;
        $this->canLike = $canLike;
    }
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNomUser()
    {
        return $this->nomUser;
    }

    /**
     * @param mixed $nomUser
     */
    public function setNomUser($nomUser)
    {
        $this->nomUser = $nomUser;
    }

    /**
     * @return mixed
     */
    public function getTitol()
    {
        return $this->titol;
    }

    /**
     * @param mixed $titol
     */
    public function setTitol($titol)
    {
        $this->titol = $titol;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getVisites()
    {
        return $this->visites;
    }

    /**
     * @param mixed $visites
     */
    public function setVisites($visites)
    {
        $this->visites = $visites;
    }

    /**
     * @return mixed
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getComents()
    {
        return $this->coments;
    }

    /**
     * @param mixed $coments
     */
    public function setComents($coments)
    {
        $this->coments = $coments;
    }

    /**
     * @return mixed
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $likes
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
    }

    public function getLastComment(){
        return $this->coments[count($this->coments) - 1];
    }

    public function setCanLike($canLike)
    {
        $this->likes = $canLike;
    }

    public function getCanLike(){
        return $this->canLike;
    }


}