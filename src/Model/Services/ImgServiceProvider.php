<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 13/05/2017
 * Time: 13:08
 */

namespace SilexApp\Model\Services;


use Silex\Application;
use SilexApp\Model\Services\Comentari;
use SilexApp\Model\Services\Img;
use SilexApp\Model\Services\Like;
use Symfony\Component\HttpFoundation\Request;

class ImgServiceProvider
{

    /*
     * Funcio que ens penja la img nova
     */
    public function creaImg(Application $app, Request $request){
        $titol = $request->get('title');
        $priv = $request->get('priv');
        $errorN = false;
        $errorI = false;
        
        //Validem TITOl
        $titolDef = str_split($titol,1);
        for ($i = 0; $i < count($titolDef); $i++) {
            if (!($titolDef[$i] >= "A" && $titolDef[$i] <= "Z") && !($titolDef[$i] >= "a" && $titolDef[$i] <= "z") && !($titolDef[$i] >= "0" && $titolDef[$i] <= "9")) {
                $errorN = true;
            }
        }

        //VALidem PRIVATE
        if ($priv == NULL){
            $priv = 0;
        }else{
            $priv = 1;
        }

        //VALIDEM IMG
        $img_path = $app['user']->upload($request, $app, 2);

        if($img_path == null){
            $errorI = true;
        }

        //OK
        if($errorN == false && $errorI == false){
            $user_id = $app['session']->get('id');

            $img = new Img(0,(int)$user_id, $request->get('title'), $img_path[0], 0, $priv,date('Y-m-d') , null, 0, 1);

            $ok =  $app['ddbb']->afegirImg($app, $img);
            //afegim les altres
            $app['ddbb']->addImgRe($app, $app['ddbb']->getLastIdImg($app), $img_path[1], 1);

            $app['ddbb']->addImgRe($app, $app['ddbb']->getLastIdImg($app), $img_path[2], 1);



            if($ok == 1){
                 //OK
                    return 0;
             }else{
                 //KO BBDD
                 return 1;
             }
        }else{
            //KO DADES
            return 2;
        }
    }


    public function actualitzaImg(Application $app, Request $request, $id){
        $titol = $request->get('titol');
        $priv = $request->get('priv');
        $date = $request->get('DataN');
        // 0-> buit 1-> ok 2-> KO
        $camps[0] = 0; //titol
        $camps[1] = 0; //img

        $ok = 1;

        //VAlidem tiotol
        if (strlen($titol) > 0) {
            $camps[0] = 1;
            $titolDef = str_split($titol, 1);
            for ($i = 0; $i < count($titolDef); $i++) {
                if (!($titolDef[$i] >= "A" && $titolDef[$i] <= "Z") && !($titolDef[$i] >= "a" && $titolDef[$i] <= "z") && !($titolDef[$i] >= "0" && $titolDef[$i] <= "9")) {
                    $camps[0] = 2;
                }
            }
        }

        //VALidem PRIVATE
        if ($priv == NULL){
            $priv = 0;
        }else{
            $priv = 1;
        }

        //VALIDEM IMG
        if(strlen($request->files->get('img') > 1) && $camps[0] != 2) {
            $camps[1] = 1;
            $img_path = $app['user']->upload($request, $app, $id+3);
            if($img_path == null){
                $camps[1] = 2;
            }
        }

        //TOT OK
        if ($camps[0] != 2 && $camps[1] != 2){
            $ok = $app['ddbb']->actualitzaImg($app, $request, $id);
        }



        if ($camps[0] == 2 || $camps[1] == 2){
            die();
            return $vectorErrors[0] = false;
        }else{
            $vectorErrors[0] = true;
            $vectorErrors[1] = $camps[0];
            $vectorErrors[2] = $camps[1];
            return $vectorErrors;
        }
    }

    /*
 * Funcio que comprova tot
 * i afegeix o treu like
 */
    public  function like (Application $app, $id){
        $can_like = true;
        $id_usu = $app['session']->get('id')['id'];

        $can_like = $app['ddbb']->likeDone($app, $id);

        if($can_like == true){
            $app['ddbb']->afegirLike($app, $id);
            $name = $app['ddbb']->getUserName($app, $id_usu);
            $img = $app['ddbb']->getImgById($app, $id);
            $usrLiked = $app['ddbb']->getUserId($app, $img->getNomUser()['usuari']);
            $app['ddbb']->insertNotification($app, 2, $img->getId(), $usrLiked , $id_usu);

            return true;
        }else{
            $app['ddbb']->delete1Like($app, $id);
            $name = $app['ddbb']->getUserName($app, $id_usu);

            $img = $app['ddbb']->getImgById($app, $id);
            $usrLiked = $app['ddbb']->getUserId($app, $img->getNomUser()['usuari']);
            $app['ddbb']->insertNotification($app, 3,$img->getId(),$usrLiked, $id_usu);
            return false;
        }
    }


    public function comentari(Application $app, $id, Request $request){
        $can_comment = true;
        $id_usu = $app['session']->get('id')['id'];

        $can_comment = $app['ddbb']->commentDone($app, $id);

        if($can_comment == true){
            $contingutFi = htmlentities($request->get('comment-text'));
            $comment = new Comentari(null, $id_usu,$id,  $contingutFi,date("Y-m-d H:i:s"));
            $app['ddbb']->afegirCommentsImg($app, $comment);
            $name = $app['ddbb']->getUserName($app, $id_usu);
            $img = $app['ddbb']->getImgById($app, $id);
            $usrLiked = $app['ddbb']->getUserId($app, $img->getNomUser()['usuari']);
            $app['ddbb']->insertNotification($app, 0, $img->getId(),$usrLiked, $id_usu);
            return true;
        }else{
            return false;
        }
    }











}