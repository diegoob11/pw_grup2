<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 16/05/2017
 * Time: 9:21
 */

namespace SilexApp\Model\Services;


class Like
{

    private $id;
    private $id_img;
    private $id_usu;
    private $like;

    /**
     * Like constructor.
     * @param $id
     * @param $id_img
     * @param $id_usu
     */
    public function __construct($id, $id_img, $id_usu)
    {
        $this->id = $id;
        $this->id_img = $id_img;
        $this->id_usu = $id_usu;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdImg()
    {
        return $this->id_img;
    }

    /**
     * @param mixed $id_img
     */
    public function setIdImg($id_img)
    {
        $this->id_img = $id_img;
    }

    /**
     * @return mixed
     */
    public function getIdUsu()
    {
        return $this->id_usu;
    }

    /**
     * @param mixed $id_usu
     */
    public function setIdUsu($id_usu)
    {
        $this->id_usu = $id_usu;
    }



}