<?php

/**
 * Created by PhpStorm.
 * User: claua
 * Date: 27/04/2017
 * Time: 20:50
 */
namespace SilexApp\Model\Services;

class User
{

    private $id;
    private $username;
    private $data_neix;
    private $contra;
    private $img_perfil;
    private $actiu;
    private $email;

    /**
     * User constructor.
     * @param $username
     * @param $data_neix
     * @param $contra
     * @param $img_perfil
     * @param $email
     */
    public function __construct($username, $data_neix, $contra, $img_perfil, $email)
    {
        $this->username = $username;
        $this->data_neix = $data_neix;
        $this->contra = $contra;
        $this->img_perfil = $img_perfil;
        $this->email = $email;
        $this->actiu = 0;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getDataNeix()
    {
        return $this->data_neix;
    }

    /**
     * @param mixed $data_neix
     */
    public function setDataNeix($data_neix)
    {
        $this->data_neix = $data_neix;
    }

    /**
     * @return mixed
     */
    public function getContra()
    {
        return $this->contra;
    }

    /**
     * @param mixed $contra
     */
    public function setContra($contra)
    {
        $this->contra = $contra;
    }

    /**
     * @return mixed
     */
    public function getImgPerfil()
    {
        return $this->img_perfil;
    }

    /**
     * @param mixed $img_perfil
     */
    public function setImgPerfil($img_perfil)
    {
        $this->img_perfil = $img_perfil;
    }

    /**
     * @return int
     */
    public function getActiu()
    {
        return $this->actiu;
    }

    /**
     * @param int $actiu
     */
    public function setActiu( $actiu)
    {
        $this->actiu = $actiu;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }



}