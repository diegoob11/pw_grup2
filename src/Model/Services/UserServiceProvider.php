<?php
/**
 * Created by PhpStorm.
 * User: claua
 * Date: 03/05/2017
 * Time: 20:07
 */

namespace SilexApp\Model\Services;
use Silex\Application;
use SilexApp\Model\Services\DataBase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use SilexApp\Model\Services\User;
use PHPMailer;
class UserServiceProvider
{
    /*
     * Funcio que fa les comprovacions de
     * les dades del nou usuari
     * retorna FALSE si no hi ha cap error
     * retorna TRUE si hi ha algun error
     */
    public function registrar(Request $request, Application $app){
        $user = $request->get('username');
        $pass = $request->get('password');
        $pass2 = $request->get('repeat_pwd');
        $email = $request->get('email');
        $name = $request->get('name');
        $date = $request->get('DataN');

        //FALSE es que no hi ha error
        $errorN = false; //user
        $errorE = false; //email
        $errorC = false; //contrasenya1
        $errorC2 = false; //Constrasenya2
        $errorD = false; //Data
        $errorI = false; //imatge


        //COMRPROVEM NOM_USU
        $userDef = str_split($user,1);
        if (count($userDef) > 20) {
            $errorN = true;
        } else {
            for ($i = 0; $i < count($userDef); $i++) {
                if (!($userDef[$i] >= "A" && $userDef[$i] <= "Z") && !($userDef[$i] >= "a" && $userDef[$i] <= "z") && !($userDef[$i] >= "0" && $userDef[$i] <= "9")) {
                    $errorN = true;
                }
            }
        }

        //COMPROVEM EMAIL
        $emailDef = str_split($email,1);
        $trobat = false;
        for ($i = 0; $i < count($emailDef); $i++){
            if(strcmp($emailDef[$i],'@') == 0){
                $trobat = true;
                break;
            }
        }
        if ($trobat == true){
            $trobat = false;
            for ($i; $i < strlen($email); $i++){
                if(strcmp($emailDef[$i],'.') == 0){
                    $trobat = true;
                    break;
                }
            }
        }
        if($trobat == false){
            $errorE = true;
        }

        //COMRPROVEM CONTRA
        $contraDef = str_split($pass,1);

        $countm = 0;
        $countM = 0;
        $Num = 0;
        if((count($contraDef)>= 6) && (count($contraDef) <= 12)){
            for ($i = 0; $i < count($contraDef); $i++){
                if ($contraDef[$i] >= "A" && $contraDef[$i] <= "Z") {
                    $countM = $countM+1;
                } else {
                    if ($contraDef[$i] >= "a" && $contraDef[$i] <= "z") {
                        $countm = $countm+1;

                    } else {
                        if ($contraDef[$i] >= "0" && $contraDef[$i] <= "9") {
                            $Num = $Num +1;
                        }
                    }
                }
            }

            if (($countM == 0) || ($countm == 0) || ($Num == 0) ) {
                $errorC = true;
            } else {

                //COMPROVEM 2a CONTRASENYA CORRECTA
                if ($pass != $pass2) {
                    $errorC2 = true;
                }
            }
        }else{
            $errorC = true;
        }

        //Comprovem data pero falta pero afegir la T al final de la data

        if (preg_match('/^'.'(\d{4})-(\d{2})-(\d{2})'. /* YYYY-MM-DDT ex: 2014-01-01T*/'$/', $date, $parts) == true)
        {
            if((time()-(60*60*24)) > strtotime($date)){
                $errorD = false;
            }else{
                $errorD = true;
            }
        }else{
            $errorD = true;
        }

        if(strlen($request->files->get('img') > 1)) {
            $img = $this->upload($request, $app,1);

            if($img == null){
                $errorI = true;
            }else{
                $errorI = false;
            }
        }else{
            $img[0] = "assets/img/avatar.png";
            $errorI = false;
        }

        if($errorC == false && $errorC2 == false && $errorE == false && $errorD == false && $errorN == false && $errorI == false){
            //0 errors -> afegim USR + img resize +
            // enviem correu
            $this->sendMail($app,$email);

            $hash = md5($request->get('password'));
            $user = new User($request->get('username'),$request->get('DataN'), $hash, $img[0], $request->get('email'));
            $app['ddbb']->afegirUser($app, $user);

            if(count($img) > 1){
                $app['ddbb']->addImgRe($app, $app['ddbb']->getLastId($app), $img[1], 0);
                $app['ddbb']->addImgRe($app, $app['ddbb']->getLastId($app), $img[2], 0);
            }


            return null;
        }else{
            $vectorErrors[0] = $errorC;
            $vectorErrors[1] = $errorC2;
            $vectorErrors[2] = $errorN;
            $vectorErrors[3] = $errorD;
            $vectorErrors[4] = $errorE;
            $vectorErrors[5] = $errorI;

           return $vectorErrors;
        }

    }

    /*
     * Comprova la user i pass per
     * a poder loggejar.se
     */
    public  function comprovaUser(Application $app, Request $request){
        $user = $request->get('username');
        $pass = $request->get('password');

        $hash = md5($pass);
        $error = $app['ddbb']->loginUser($app, $user, $hash);

        if($error == 2){
            $id = $app['ddbb']->getUserId($app, $user);
            $app['session']->set('id',$id);
        }

        return $error;
    }

    public function activaUser(Application $app){
       $id = $app['ddbb']->getLastId($app);
       $ok = $app['ddbb']->activaUser($app, $id);

        if($ok == 1){
            $app['session']->set('id',$id);
            return 1;
        }else{
            return 0;
        }
    }

    /*
     * Les caselles 4, 5, 6 del vector d'errors son les
     * que ens avisen si esta buid (True vol dir que esta buit)
     */
    public function comprovaNouPerfil (Application $app, Request $request){

        $user = $request->get('username');
        $pass = $request->get('password');
        $date = $request->get('DataN');
        //FALSE es que no hi ha error
        $errorN = false; //user
        $errorC = false; //contrasenya
        $errorD = false; //Data
        $errorI = false; //IMG

        $vectorErrors[0] = false;
        $vectorErrors[1] = false;
        $vectorErrors[2] = false;

        $vectorErrors[3] = true;
        $vectorErrors[4] = true;
        $vectorErrors[5] = true;
        $vectorErrors[6] = true;


        //NO BUIT
        if (strlen($user) > 0){
            $vectorErrors[4] = false;

            $userDef = str_split($user,1);
            if (count($userDef) > 20) {
                $errorN = true;
            } else {
                for ($i = 0; $i < count($userDef); $i++) {
                    if (!($userDef[$i] >= "A" && $userDef[$i] <= "Z") && !($userDef[$i] >= "a" && $userDef[$i] <= "z") && !($userDef[$i] >= "0" && $userDef[$i] <= "9")) {
                        $errorN = true;
                    }
                }
            }
        }else{
            $errorN = false;
            $vectorErrors[4] = true;
        }

        //NO BuiT
        if(strlen($pass) > 0){

            $vectorErrors[5] = false;

            $contraDef = str_split($pass,1);

            $countm = 0;
            $countM = 0;
            $Num = 0;
            if((count($contraDef)>= 6) && (count($contraDef) <= 12)){
                for ($i = 0; $i < count($contraDef); $i++){
                    if ($contraDef[$i] >= "A" && $contraDef[$i] <= "Z") {
                        $countM = $countM+1;
                    } else {
                        if ($contraDef[$i] >= "a" && $contraDef[$i] <= "z") {
                            $countm = $countm+1;

                        } else {
                            if ($contraDef[$i] >= "0" && $contraDef[$i] <= "9") {
                                $Num = $Num +1;
                            }
                        }
                    }
                }

                if (($countM == 0) || ($countm == 0) || ($Num == 0) ) {
                    $errorC = true;
                }

            }else{
                $errorC = true;
            }
        }else{
            $errorC = false;
            $vectorErrors[5] = true;
        }

        //BUIT
        if (strlen($date) < 1){
            $errorD = false;
            $vectorErrors[6] = true;
        }else{
            $vectorErrors[6] = false;

            if((time()-(60*60*24)) > strtotime($date)){
                $errorD = false;
            }else{
                $errorD = true;
            }
        }

        //NO BUIT IMG
        if(strlen($request->files->get('img') > 1)) {
            $vectorErrors[3] = false;
        }


        $vectorErrors[0] = $errorN;
        $vectorErrors[1] = $errorC;
        $vectorErrors[2] = $errorD;

        if($errorC == false && $errorD == false && $errorN == false && $errorI == false){
            //0 errors
            $miss = $this->actualitzaUser($app, $vectorErrors, $request);
            return $miss;
        }else{
            return $vectorErrors;
        }
    }

    /*
     * Si vector [i] == false vol dir
     * que NO es buit
     */
    public function actualitzaUser(Application $app, $vect, $request){
        $okN = 1;
        $okP = 1;
        $okD = 1;
        $okI = 1;

        //IMG
        if($vect[3] == false){
            $img = $this->upload($request, $app,0);

            if($img == null){
                $okI = 0;
            }


        }
        //USERNAME
        if($vect[4] == false){
            $okN = $app['ddbb']->actualitzaUsername($app, $request->get('username'));
        }

        //PASSWORD
        if($vect[5] == false){
            $hash = md5($request->get('password'));
            $okP =  $app['ddbb']->actualitzaUserPass($app, $hash);

        }
        //DATE
        if($vect[6] == false){
            $okD = $app['ddbb']->actualitzaUserDate($app, $request->get('DataN'));
        }

        //ERROR
        if($okN == 0 || $okD == 0 || $okP == 0 || $okI == 0){
            return 0;
        }else{
            //OK
            return 1;
        }
    }

    /*
     * Funcio que retorna
     * un array amb els 3 path de les 3 img
     * o null
     * OPCIO = 0-> reg, 1->editPerfil , 2->newPic, 3-> ediPic
     */
    public function upload (Request $request, Application $app, $opcio){
        $file = $request->files->get('img');

        $myFile = $file;

        $tipo = exif_imagetype($file);

        if ($tipo == IMAGETYPE_JPEG || $tipo == IMAGETYPE_PNG || $tipo == IMAGETYPE_GIF){
            switch ($opcio){
                case 0:
                    //LOGEJAT -> edicio perfil
                    $img_path = $this->upload_Image($file, $app, $app['ddbb']->getUserName($app, $app['session']->get('id')['id'])['usuari'], 1);
                    break;
                case 1:
                    //REGISTRE
                    $usr = $request->get('username'); //USERNAME
                    $img_path = $this->upload_Image($file, $app, $usr,1);
                    break;
                case 2:
                    //NEW PIC
                    $img_path = $this->upload_Image($file, $app, $app['ddbb']->getLastIdImg($app)+1,0);
                    break;
                default:
                    //EDITA PIC
                    $id_img = $opcio-3;
                    $img_path = $this->upload_Image($file, $app, $id_img,0);
                    break;
            }

            return $img_path;
        }else{
            return null;
        }
    }

    /*
     * GUARFDA Les 3 img
     * amb els seus resize pertinents
     */
    public function upload_Image (UploadedFile $img_path, Application $app, $name, $opcio){
        $myFile = $img_path;

        $success = move_uploaded_file($img_path->getRealPath(),'assets/img/'. $name . '.jpg');

        $imagine = new Imagine();
        $image1 = $imagine->open('assets/img/' . $name .'.jpg');
        $image2 = $imagine->open('assets/img/' . $name .'.jpg');
        $image1 -> resize(new Box(100,100))
            ->save('assets/img/'.'1_'.$name. '.jpg');
        $image2 -> resize(new Box(400,300))
            ->save('assets/img/'.'2_'.$name . '.jpg');

        if($opcio == 1){
            $image3 = $imagine->open('assets/img/' . $name.'.jpg');
            $image3 -> resize(new Box(30,30))
                ->save('assets/img/'.'3_'.$name.'.jpg');
        }
        if($success == true){
            $vectorImg[0] = "assets/img/$name.jpg";
            $vectorImg[1] = "assets/img/1_$name.jpg";
            $vectorImg[2] = "assets/img/2_$name.jpg";
            return $vectorImg;
        }else{
            return null;
        }

    }

    /*
     * Envia mail
     */
    public function sendMail (Application $app, $email){
        $mail = new PHPMailer;

        $mail->isSMTP();
        $mail->SMTPAuth = true;
        $mail->SMTPSecure ="tls";
        $mail->Host = "smtp.gmail.com";
        $mail->isSMTP();
        $mail->Username = "projectesweb2@gmail.com";
        $mail->Password = "projectesweb";
        $mail->Port = 587;

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->SetFrom("rcalzada13@gmail.com", 'PWGRAM');
        $mail->AddReplyTo("rcalzada13@gmail.com",'PWGRAM');
        $mail->AddAddress($email);

        $misatje = $app['twig']->render('mail.twig', array(

        ));

        $mail->isHTML(true);
        $mail->Subject ='Activate your account';
        $mail->Body = $misatje;

        if($mail->Send()){
            return true;
        }else{
            return false;
        }

    }

    public function organitzaNotis(Application $app, $noti){

        $vectorNotis = null;
        $task = "";
        if(count($noti) > 0){
            for ($i = 0  ; $i < count($noti); $i++){

                $user = $app['ddbb']->getUserName($app, $noti[$i]['id_commented'])['usuari'];

                $img_titol = $app['ddbb']->getMyTitleImg($app, $noti[$i]['id_img'])[0]['titol'];

                $CL = $noti[$i]['CL'];
                switch ($CL){
                    case 0:
                        $task = "commented your picture:";
                        break;
                    case 1:
                        $task = "deleted a comment of your picture:";
                        break;
                    case 2:
                        $task = "liked your immage:";
                        break;
                    case 3:
                        $task = "disliked your immage:";
                        break;
                }
                $vectorNotis[$i]['text'] = "$user has $task $img_titol";
                $vectorNotis[$i]['id'] = $noti[$i]['id'];
            }
            return $vectorNotis;
        }else{
            return null;
        }
        
    }
}