var counter = 0;

$(document).ready(function() {
    $('#form-images').submit(function(event) {
        counter++;
        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/loadMoreImages',
            data: data,
            dataType: 'json',
            encode: true
        })
        .done(function(response) {
            var list = document.getElementById('items-list');
            var array = response.slice(5*counter, 5*counter + 5);
            console.log(array);
            array.forEach(function (element){
                var resp = JSON.parse(element);
                console.log(resp);
                var li = document.createElement('li');
                li.setAttribute("id", "image-element");

                var figure = document.createElement('figure');
                figure.setAttribute("id", "home-fig");

                var img = document.createElement('img');
                img.setAttribute("width", "300");
                img.setAttribute("height", "300");
                img.setAttribute("src", resp.path);

                var figcapt = document.createElement('figcaption');
                figcapt.setAttribute("id", "home-figcapt");

                var h3 = document.createElement('h3');
                var a = document.createElement('a');
                a.setAttribute("href", "viewImg/".concat(resp.id));
                a.appendChild(document.createTextNode(resp.titol));
                h3.appendChild(a);
                var p = document.createElement('p');
                p.appendChild(document.createTextNode("Posted by "));
                var a2 = document.createElement('a');
                a2.setAttribute("href", "Profile/".concat(resp.nomUser));
                a2.appendChild(document.createTextNode("@".concat(resp.nomUser)));
                p.appendChild(a2);
                p.appendChild(document.createTextNode(" on ".concat(resp.data)));
                p.appendChild(document.createElement('br'));
                var span = document.createElement('span');
                span.setAttribute("class", "glyphicon glyphicon-thumbs-up");
                span.setAttribute("aria-hidden", "true");
                p.appendChild(span);
                p.appendChild(document.createTextNode(resp.likes));
                var span2 = document.createElement('span');
                span2.setAttribute("class", "glyphicon glyphicon-bookmark");
                span2.setAttribute("aria-hidden", "true");
                p.appendChild(span2);
                if(resp.coments == null) resp.coments = 0;
                p.appendChild(document.createTextNode(resp.coments.length));
                var span3 = document.createElement('span');
                span3.setAttribute("class", "glyphicon glyphicon-eye-open");
                span3.setAttribute("aria-hidden", "true");
                p.appendChild(span3);
                p.appendChild(document.createTextNode(resp.visites));

                figcapt.appendChild(h3);
                figcapt.appendChild(p);

                figure.appendChild(img);
                figure.appendChild(figcapt);

                li.appendChild(figure);
                li.appendChild(document.createElement('br'));

                var form = document.createElement('form');
                form.setAttribute("method", "POST");
                form.setAttribute("action", "comment/".concat(resp.id));
                var div = document.createElement('div');
                var input = document.createElement('input');
                input.setAttribute("type", "text");
                input.setAttribute("id", "comment-text");
                input.setAttribute("name", "comment-text");
                input.setAttribute("placeholder", "Express yourself here...");
                var button = document.createElement('button');
                button.setAttribute("type", "submit");
                button.setAttribute("class", "secondary-button");
                button.setAttribute("id", "comment-button");
                button.setAttribute("name", "comment-button");
                button.setAttribute("value", "comment");
                button.appendChild(document.createTextNode("comment"));
                div.appendChild(input);
                div.appendChild(button);
                form.appendChild(div);

                li.appendChild(form);

                var form2 = document.createElement('form');
                form2.setAttribute("method", "POST");
                form2.setAttribute("action", "like/".concat(resp.id));
                if(resp.canLike == true){
                    var div2 = document.createElement('div');
                    var button2 = document.createElement('button');
                    button2.setAttribute("type", "submit");
                    button2.setAttribute("class", "secondary-button");
                    button2.setAttribute("id", "like-button");
                    button2.setAttribute("name", "like-button");
                    button2.setAttribute("value", "like");
                    var spanLike = document.createElement('span');
                    spanLike.setAttribute("class", "glyphicon glyphicon-thumbs-up");
                    spanLike.setAttribute("aria-hidden", "true");
                    button2.appendChild(spanLike);
                    button2.appendChild(document.createTextNode(" Like"));

                    div2.appendChild(button2);
                    form2.appendChild(div2);
                }else{
                    var div3 = document.createElement('div');
                    var button3 = document.createElement('button');
                    button3.setAttribute("type", "submit");
                    button3.setAttribute("class", "secondary-button");
                    button3.setAttribute("id", "dislike-button");
                    button3.setAttribute("name", "dislike-button");
                    button3.setAttribute("value", "dislike");
                    var spanLike = document.createElement('span');
                    spanLike.setAttribute("class", "glyphicon glyphicon-thumbs-down");
                    spanLike.setAttribute("aria-hidden", "true");
                    button3.appendChild(spanLike);
                    button3.appendChild(document.createTextNode(" Dislike"));

                    div3.appendChild(button3);

                    form2.appendChild(div3);
                }

                li.appendChild(form2);
                list.appendChild(li);
            });

        })
        event.preventDefault();
    });
});