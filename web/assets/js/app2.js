var counter = 0;

$(document).ready(function() {
    $('#form-comments').submit(function(event) {
        counter++;
        var data = $(this).serialize();
        $.ajax({
            type: 'POST',
            url: '/loadMoreComments/'+ document.getElementById("1").className,
            data: data,
            dataType: 'json',
            encode: true
        })
        .done(function(response) {
            var list = document.getElementById('comments-div');
            var array = response.slice(counter*3, counter*3 + 3);
            array.forEach(function (element){
                var resp = JSON.parse(element);
                var div = document.createElement('div');
                var hr = document.createElement('hr');
                hr.setAttribute("class", "cool-hr");
                var p = document.createElement('p');
                var strong = document.createElement('strong');
                strong.appendChild(document.createTextNode("@".concat(resp.nomUser)));
                strong.appendChild(document.createTextNode(" said: "));
                p.appendChild(strong);
                p.appendChild(document.createTextNode(resp.comentari));

                div.appendChild(hr);
                div.appendChild(p);
                list.appendChild(div);
            });

        })
        event.preventDefault();
    });
});