function checkUsername(){
    var unm = /^[0-9a-zA-Z].{1,20}$/;
    if(document.getElementById("username").value.match(unm)){
        $('#usernameIncorrect').hide();
    }else{
        $('#usernameIncorrect').show();
    }
}

function checkPassword(){
    var passw = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;
    console.log(document.getElementById("password").value.match(passw));
    if(document.getElementById("password").value.match(passw)){
        $('#passwordIncorrect').hide();
    }else{
        $('#passwordIncorrect').show();
    }
}

function checkEqualPasswords(){
    if(document.getElementById('password').value == document.getElementById('repeat_pwd').value){
        $(' #passwordsNoMatch').hide();

    }else{
        $('#passwordsNoMatch').show();
    }
}

function checkEmail(){
    var eml = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(document.getElementById("email").value.match(eml)){
        $('#emailIncorrect').hide();
    }else{
        $('#emailIncorrect').show();
    }
}

function checkBirthdate(){
    var date = /^([\+-]?\d{4}(?!\d{2}\b))((-?)((0[1-9]|1[0-2])(\3([12]\d|0[1-9]|3[01]))?|W([0-4]\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\d|[12]\d{2}|3([0-5]\d|6[1-6])))([T\s]((([01]\d|2[0-3])((:?)[0-5]\d)?|24\:?00)([\.,]\d+(?!:))?)?(\17[0-5]\d([\.,]\d+)?)?([zZ]|([\+-])([01]\d|2[0-3]):?([0-5]\d)?)?)?)?$/;
    if(document.getElementById("DataN").value.match(date)){
        $('#birthdateIncorrect').hide();
    }else{
        $('#birthdateIncorrect').show();
    }
}